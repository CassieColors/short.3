#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h> 
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/shm.h>
#include <time.h>
#include <signal.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>

typedef struct SharedMemory{
	long seconds;
	long nanoseconds;
	long shMsg[3];
} SharedMemory;

// Prototypes
void destroySharedMemory();
void interruptHandler(int);
void findTime();
void writeLog(char*);

// Global variables that need to be accessed in interrupt handler
int* PID;
SharedMemory* dataPointer;
int structId;
int runningProcesses = 1;
int numberOfChildren = 5;
time_t currentTime;
struct tm* timeInfo;
sem_t* semaphores;
FILE* logFile;
char* msg;

int main (int argc, char *argv[]) {
	int hflag = 0;
	int options;

	findTime();
	char* fileName;
	char defaultLogs[20];
	sprintf(defaultLogs, "%d.%d-logs.txt", (timeInfo->tm_mon + 1), timeInfo->tm_mday);
	fileName = defaultLogs;

	char errorString[100];
	unsigned int timeout = 20;
	SharedMemory data;
	dataPointer = &data;
	int structKey = 1234;
	int totalProcesses = 0;
	msg = malloc(100 * sizeof(char));

	// Check for any options passed with the executable and respond accordingly	
	while ((options = getopt (argc, argv, "hl:t:s:")) != -1){
		switch (options){
			case 'h':
				hflag = 1;
				break;
			case 'l':
				fileName = optarg;
				break;
			case 't':
				timeout = atoi(optarg);
				break;
			case 's':
				numberOfChildren = atoi(optarg);
				break;
			case '?':
				// If the flag wasn't recognized, print a list of available options
        			if (isprint (optopt))
					// If the flag passed was a number, the user was probably trying to pass a negative argument
					if(isdigit(optopt)){
						errno = EINVAL;
						sprintf(errorString, "%s: Error: Negative argument", argv[0]);
						perror(errorString);
						fprintf(stdout, "Enter \'%s -h\' to view a description of arguments for %s\n", argv[0], argv[0]);

          				} else {
						fprintf (stdout, "Available options\n-- \'h\' (view arguments).\n-- \'l\' (filename)\n-- \'t\' (timer)\n-- \'s\' (children)\n", optopt);
              				}
				return 1;
			default:
				abort();
		}
	}
	// If the hflag is detected, print help message and exit
	if(hflag){
		fprintf(stdout, "%s has three optional arguments.\nUse -l followed by a full filename to specify a filename.\nUse -t followed by a positive integer to specify a timeout limit in seconds.\nUse -s followed by a positive integer to set the number of slaves for this run.\n", argv[0]);
		return 0;
	}
	if(argc > 5) {
		errno = E2BIG;
		sprintf(errorString, "%s: Error", argv[0]);
		perror(errorString);
		fprintf(stdout, "Enter \'%s -h\' to view a description of arguments for %s\n", argv[0], argv[0]);
		return 1;
	}

	//  Use variables that can be altered by the command line arguments
	alarm(timeout);
	PID = malloc(numberOfChildren * sizeof(int));

	// Set signal handlers
        if(signal(SIGINT, interruptHandler) == SIG_ERR){
		sprintf(errorString, "%s: Error catching SIGINT", argv[0]);
		perror(errorString);
	        exit(1);
        }
        if(signal(SIGALRM, interruptHandler) == SIG_ERR){
                sprintf(errorString, "%s: Error catching SIGALRM", argv[0]);
		perror(errorString);
		exit(1);
        }
	if(signal(SIGABRT, interruptHandler) == SIG_ERR){
		sprintf(errorString, "%s: Error catching SIGABRT", argv[0]);
		perror(errorString);
		exit(1);
	}

	// Create and attach shared memory for struct so that it can be accessed by all processes
	structId = shmget(structKey, sizeof(data), IPC_CREAT | 0666);
	if(structId == -1){
		sprintf(errorString, "%s: Error: Failed to allocate shared memory", argv[0]);
		perror(errorString);
		destroySharedMemory();
		return 1;
	}
	dataPointer = shmat(structId, NULL, 0);
	if(dataPointer == (SharedMemory*)-1){
		sprintf(errorString, "%s: Error: Failed to attach shared memory", argv[0]);
		perror(errorString);
		destroySharedMemory();
		return 1;
	}

	// Iniitialize semaphores
	semaphores = sem_open("sems", O_CREAT | O_EXCL, 0666, 1);
	if(semaphores == SEM_FAILED){
		sprintf(errorString, "%s: Error: Failed to create semaphore", argv[0]);
		perror(errorString);
		abort();
	}

	if(sem_close(semaphores) < 0){
		sprintf(errorString, "%s: Error: Failed to close semaphore", argv[0]);
		perror(errorString);
		abort();
	}

	// Fill array of child process PIDs and shared Messages array with -1's to initiate them, and initialize clock to zero
	int j, p;
	for(j = 0; j < numberOfChildren; j++){
		PID[j] = -1;
	}
	dataPointer->seconds = 0;
	dataPointer->nanoseconds = 0;
	for(p = 0; p < 3; p++){
		dataPointer->shMsg[p] = -1;
	}
	
	// Open file and check that this was successful
	logFile = fopen(fileName, "a");
	if(logFile == NULL){
		sprintf(errorString, "%s: Error opening log file", argv[0]);
		perror(errorString);
		destroySharedMemory();
		return 1;
	}
	sprintf(msg, "\n\nInitiating oss...\n");
	writeLog(msg);


	// Start originaly requested processes
	int k, m;
	int status;
	int pid;
	for(k = 0; k < numberOfChildren; k++){
		runningProcesses++;
		if((PID[k] = fork()) == -1){
		// -1 means the fork failed, handle the error
			sprintf(errorString, "%s: Error: Forking error", argv[0]);
			perror(errorString);
			abort();
		} else if(PID[k] == 0){
		// 0 means this is the child, convert integers to strings and call palin with exec
			char processIDChar[5];
			sprintf(processIDChar, "%d", k);
			char* args[] = {"./user", processIDChar, NULL};
			
			execvp("./user", args);
			
			// If this block is reached there was an error with exec
			sprintf(errorString, "%s: Exec error within child %d", argv[0], (k + 1));
			perror(errorString);
			abort();
		}
		// If neither of the two conditions above were met, it's the parent, which starts the next iteration
		sprintf(msg, "Child %d with PID %ld was initialized at %ld:%ld.\n", (k + 1), (long)getpid(), 0, 0);
		writeLog(msg);
		totalProcesses++;
	}

	// Generate new processes if any of the others have stopped and increment the clock
	while(totalProcesses <= 100 && dataPointer->seconds < 2){
		// If all three message fields are set in shared memory, that process is terminating and the message can be read
		if((dataPointer->shMsg[0] != -1) && (dataPointer->shMsg[1] != -1) && (dataPointer->shMsg[2] != -1)){
			int k;
			long childSeconds = dataPointer->shMsg[0];
			long childNanos = dataPointer->shMsg[1];
			long childID = dataPointer->shMsg[2];

			sprintf(msg, "At %ld:%ld child process %d with PID %ld terminated with internal time %ld:%ld\n", dataPointer->seconds, dataPointer->nanoseconds, (childID + 1), PID[childID], childSeconds, childNanos);
			writeLog(msg);
	
			fprintf(stdout, "At %ld:%ld child process %ld with PID %ld terminated with time %ld:%ld in slave\n", dataPointer->seconds, dataPointer->nanoseconds, (childID + 1), PID[childID], childSeconds, childNanos);

			// Wait for the terminating process to be completely finished
			waitpid(PID[childID], &status, 0);
	
			if((PID[childID] = fork()) == -1){
				sprintf(errorString, "%s: Error: Forking error", argv[0]);
                     		perror(errorString);
                       		abort();
			} else if(PID[childID] == 0){
                      		char processIDChar[5];
               	        	sprintf(processIDChar, "%d", childID);
       	               		char* args[] = {"./user", processIDChar, NULL};
				
				execvp("./user", args);
	
       	               		// If this block is reached there was an error with exec
       	               		sprintf(errorString, "%s: Exec error within child %d", argv[0], (childID + 1));
       	               		perror(errorString);
                      		abort();
			}
			// Parent sets shMsg back to empty
			for(k = 0; k < 3; k++){
				dataPointer->shMsg[k] = -1;
			}
			sprintf(msg, "Child %d with PID %ld was initialized at %ld:%ld.\n", (k + 1), (long)getpid(), dataPointer->seconds, dataPointer->nanoseconds);
			writeLog(msg);	
			totalProcesses++;
		}
		
		// Increment oss clock
		dataPointer->nanoseconds += 1900;
		if(dataPointer->nanoseconds >= 1000000000){
			dataPointer->nanoseconds = (dataPointer->nanoseconds % 1000000000);
			dataPointer->seconds++;
		}
	}

	if(dataPointer->seconds >= 2){
		fprintf(stdout, "2 seconds elapsed in %s. Shutting down...\n",  argv[0]);
		sprintf(msg, "2 seconds elapsed in %s.\n", argv[0]);
		writeLog(msg);
		alarm(1);
	}
	if(totalProcesses >= 100){
		fprintf(stdout, "%s generated 100 processes. Shutting down...\n", argv[0]);
       		sprintf(msg, "%s generated 100 processes.\n", argv[0]);
        	writeLog(msg);
		abort();
	}

	// Wait for all child processes to end and clear them from the PID array as they do
	while(runningProcesses > 0){
		pid = wait(&status);
		for(m = 0; m < numberOfChildren; m++){
			if(PID[m] == pid){
				PID[m] = -1;	
			}
		}
		runningProcesses--;
	}

	// Detach and remove shared memory, close log file, and deallocate dynamic PID array
	destroySharedMemory();
	fclose(logFile);
	free(PID);
	free(msg);
	return 0;
}

void interruptHandler(int SIGNAL){
	int i = 0;
	// Check which signal was receieved and print appropriate message
	if(SIGNAL == SIGINT){
		fprintf(stderr, "Ctrl-C interrupt detected. Terminating all processes and deallocating shared memory...\n");
	} else if(SIGNAL == SIGALRM){
		fprintf(stderr, "Timer expired. Terminating all processes and deallocating shared memory...\n");
	}
	if(SIGNAL == SIGALRM || SIGNAL == SIGABRT){
		// Kill all child processes
		for(i = 0; i < numberOfChildren; i++){
			if(PID[i] != -1){
				if (kill(PID[i], SIGNAL) == 0){
					runningProcesses--;
				}	
			}
		}
	}
	// Sleep briefly so that memory destruction message prints last
	sleep(1);
        
	// Detach and remove shared memory
	destroySharedMemory();
	exit(1);
}

void destroySharedMemory(){
	fprintf(stdout, "Destroying shared memory...\n");
	shmdt(dataPointer);
	shmctl(structId, IPC_RMID, NULL);
	free(PID);
	free(msg);
	sem_unlink("sems");
	sem_close(semaphores);
}

void findTime(){
        time(&currentTime);
        timeInfo = localtime(&currentTime);
}

void writeLog(char* msg){
	char errorString[100];
	if(fputs(msg, logFile) < 0){
		sprintf(errorString, "oss: Error writing to log file\n");
		perror(errorString);
		abort();	
	}
}
