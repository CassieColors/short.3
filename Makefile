EXECS = oss user
FLAGS = -pthread

all: $(EXECS)

oss: oss.c
	gcc $(FLAGS) -o oss oss.c

user: user.c
	gcc $(FLAGS) -o user user.c

clean:
	rm -f *.out
	rm -f $(EXECS)
