#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/shm.h>
#include <time.h>
#include <signal.h>
#include <fcntl.h>
#include <semaphore.h>
#include <sys/stat.h>

typedef struct SharedMemory{
	long seconds;
	long nanoseconds;
	long shMsg[3];
} SharedMemory;

// Prototypes
void findTime();
void interruptHandler(int);

// Global variable for handling interrupts
SharedMemory* dataPointer;
int processNumber = 0;
time_t currentTime;
struct tm* timeInfo;
char errorString[100];
sem_t* semaphores;

int main (int argc, char *argv[]) {

	if(signal(SIGINT, interruptHandler) == SIG_ERR){
		sprintf(errorString, "%s: Error catching SIGINT", argv[0]);
                perror(errorString);
		exit(1);
        }
        if(signal(SIGALRM, interruptHandler) == SIG_ERR){
                sprintf(errorString, "%s: Error catching SIGALRM", argv[0]);
		perror(errorString);
		exit(1);
        }
	if(signal(SIGABRT, interruptHandler) == SIG_ERR){
		sprintf(errorString, "%s: Error catching SIGABRT", argv[0]);
		perror(errorString);
		exit(1);
	}

	SharedMemory data;
	dataPointer = &data;
	int structKey = 1234;
	int structId;
	processNumber = atoi(argv[1]);

	// Retrieve id for shared memory block and attach to it
	structId = shmget(structKey, sizeof(data), IPC_CREAT | 0666);
        if(structId == -1){
                sprintf(errorString, "%s: Error: Failed to locate shared memory", argv[0]);
                perror(errorString);
                abort();
        }
        dataPointer = shmat(structId, NULL, 0);
        if(dataPointer == (SharedMemory*)-1){
                sprintf(errorString, "%s: Error: Failed to attach shared memory", argv[0]);
                perror(errorString);
                abort();
        }
	
	// Retrieve semaphores
	semaphores = sem_open("sems", O_RDWR);
	if(semaphores == SEM_FAILED){
		sprintf(errorString, "%s: Error: Failed to retrieve semaphore", argv[0]);
		perror(errorString);
		abort();
	}

	// Find time of oss and add a random duration to it to set the end  time of this child
	srand((unsigned)time(NULL));
	int duration = ((rand() * (processNumber + 1)) % 1000000);
	int endSeconds = dataPointer->seconds;
	int endNanos = (dataPointer->nanoseconds + duration);
	if(endNanos > 1000000000){
		endNanos = (endNanos % 1000000000);
		endSeconds++;
	}
	
	// Loop on a flag that only flips when the process was able to send its message to the parent
	int executing = 1;
	while(executing){
		if(sem_wait(semaphores) < 0){
			sprintf(errorString, "%s: Error: Failed to wait on semaphore", argv[0]);
			perror(errorString);
			abort();
		}

		// Critical section
		if(((dataPointer->seconds * 1000000000) + dataPointer->nanoseconds) >= ((endSeconds * 1000000000) + endNanos)){
			if(dataPointer->shMsg[0] == -1){
				fprintf(stderr, "Process %d with PID %ld is ready to terminate\n", (processNumber + 1), (long)getpid());
				dataPointer->shMsg[0] = dataPointer->seconds;
				dataPointer->shMsg[1] = dataPointer->nanoseconds;
				if(dataPointer->shMsg[1] > 1000000000){
					dataPointer->shMsg[1] = (dataPointer->shMsg[1] % 1000000000);
					dataPointer->shMsg[0]++;
				}
				dataPointer->shMsg[2] = processNumber;
				executing = 0;;
			}
		}

		// After critical section wake next process
		if(sem_post(semaphores) < 0){
			sprintf(errorString, "%s: Error: Failed to signal on semaphore", argv[0]);
			perror(errorString);
			abort();
		}
	}
	fprintf(stderr, "Process %d with PID %ld successfully posted it's termination message.\n", (processNumber + 1), (long)getpid());
	// Detach shared memory
	shmdt(dataPointer);
	return 0;
}

void findTime(){
	time(&currentTime);
	timeInfo = localtime(&currentTime);
}

// Deliver message based on interrupt and detach memory
void interruptHandler(int SIGNAL){
	if(SIGNAL == SIGALRM){
		fprintf(stderr, "Timeout alert received from parent. Process %d with PID %ld terminating...\n", (processNumber + 1), (long)getpid());
	} else if(SIGNAL == SIGABRT){
 		fprintf(stderr, "The program was aborted. Process %d with PID %ld terminating...\n", (processNumber + 1), (long)getpid());
	} else {
		fprintf(stderr, "Interrupt received from parent. Process %d with PID %ld terminating...\n", (processNumber + 1), (long)getpid());
	}
	shmdt(dataPointer);
	sem_unlink("sems");
	sem_close(semaphores);
	exit(1);
}
